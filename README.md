# Predicta API - Your custom prediction model

## Content of this document

1. [Basic use case](#markdown-header-basic-use-case)
    - [Quickstart](#markdown-header-quickstart)
        * [Setup using docker-compose](#markdown-header-setup-using-docker-compose)
        * [Manual setup](#markdown-header-manual-setup)
    - [Using the API](#markdown-header-using-the-api)
        * [Querying the authentication API](#markdown-header-querying-the-authentication-api)
        * [Querying the prediction model API](#markdown-header-querying-the-prediction-model-api)
    - [Securing the app](#markdown-header-securing-the-app)
2. [Real use case](#markdown-header-real-use-case)
    - [Adapting the implementation](#markdown-header-adapting-the-implementation)
    - [Scaling the architecture](#markdown-header-scaling-the-architecture)

## 1. Basic use case

### Quickstart

First, you need to clone the repository in order to get working:

```sh
$ git clone git@bitbucket.org:gtitze/paretos-backend-task.git
$ cd paretos-backend-task
```

#### Setup using docker-compose

The easiest and recommended way to setup Predicta API is using docker compose:

```sh
$ docker-compose up
```

By default, the app launches on `http://localhost:5000`. You can access the documentation of
Predicta API at [http://localhost:5000/](http://localhost:5000/).

Find further information on how to consume Predicta API for testing purposes in the
[Using the API section](#markdown-header-using-the-api).

#### Manual setup

If you want to run Predicta API manually, you need to make sure, that it has a database avaiable
during runtime. The easiest way to get a Postgresql instance up and running is using docker with
the following command:

```sh
$ docker run -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres
```

To enable it to connect to the database, you must set a connection string using the environment
variable `SQLALCHEMY_DATABASE_URI`. Find more information on how to compose the connection string
in the [documentation of flask\_sqlalchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/#connection-uri-format).
An example for setting the configuration for a Postgresql database and the working configuration
for the docker Postgresql instance we just created looks as follows:

```sh
$ export SQLALCHEMY_DATABASE_URI=postgresql://postgres:postgres@localhost
```

Furthermore, you need to install the dependencies using the `requirements.txt` file and it is
recommended to use a virtual environments for setting up each projects dependencies. Both tasks
can be achieved by the following commands:

```sh
$ python3 -m venv venv && source venv/bin/activate && pip install -r requirements.txt
```

You are now ready to run Predicta API by using the flask command. For further options while running
flask consider the [Flask User's Guide](https://flask.palletsprojects.com/en). A simple command to
run the application looks like this:

```sh
$ flask run
```

At the same time, it is possible to invoke Predicta API directly via python:

```sh
$ python3 app.py
```

In order to access the API documentation, set the `PREDICTA_DOC_PATH` variable to the desired path.
If you don't set the doc path, the documentation will not be made available via the server which
might be prefereed in production. To make the documentation available at the root, you can execute
the folowing command to set the environment variable:

```sh
$ export PREDICTA_DOC_PATH=/
```

### Using the API

You can find a documentation at the path supplied via the environment variable `PREDICTA_DOC_PATH`.
If you used the setup via `docker-compose`, you can access the documentation at the root of
Predicta API [http://localhost:5000/](http://localhost:5000).

#### Querying the authentication API

To register a new user, you can use the following command:

```sh
$ curl -X POST -H "Content-Type:application/json" \
      -d '{"user_name":"testuser","password":"testpw"}' \
      http://localhost:5000/auth/register
```

Now you can log in as the user:

```sh
$ curl -X POST -H "Content-Type:application/json" \
      -d '{"user_name":"testuser","password":"testpw"}' \
      http://localhost:5000/auth/login

# To pipe the access_token to the PREDICTA_ACCESS_TOKEN environment variable for easing the code,
# you can use our parse_access_token.py helper:

$ export PREDICTA_ACCESS_HEADER="Authorization: Bearer $(curl -X POST -H "Content-Type:application/json" \
      -d '{"user_name":"testuser","password":"testpw"}' \
      http://localhost:5000/auth/login \
      | python parse_access_token.py)"
```

#### Querying the prediction model API

Requests to the prediction model API must be made by a user authenticated via JSON Web Tokens (JWT).
In order to authenticate during the request, a `Authorization` HTTP header of the form
`Bearer <access_token>` which can be retrieved via the [authentication API](#markdown-header-querying-the-authentication-api).


The following snippet returns all models created by the user:

```sh
$ curl -H "$PREDICTA_ACCESS_HEADER" http://localhost:5000/model
```

To create a new model, you can use the following code:

```sh
$ curl -X POST -H "$PREDICTA_ACCESS_HEADER" -H "Content-Type:application/json" \
      -d '{"model_name":"testmodel","input_columns":["col_a","col_b"],"input_weights":[1.0,2.0]}' \
      http://localhost:5000/model
```

In order to create a prediction for the model, a dictionary containing the column names as keys and
the `float` input parameters as values needs to be provided in the request body. Data columns that
are not used by the model will be ignored.

```sh
# Potential data for the prediction:
# {
#     "col_a": 1.0,
#     "col_b": 10.0,
#     "unused_col": 100,
# }

$ curl -X POST -H "$PREDICTA_ACCESS_HEADER" -H "Content-Type:application/json" \
      -d '{"col_a":1.0,"col_b":10.0,"unused_col":100.0}' \
      http://localhost:5000/model/<model_id>/prediction
```

### Securing the app

In order to secure the app for production, it should be deployed by using a WSGI server. It should
therefore never be directly invoked via the shell.

Furthermore, a secure key needs to be supplied for JWT using the `JWT_SECRET_KEY` environment variable:

```sh
$ export JWT_SECRET_KEY=<secret_key>
```

## Real use case

For the real use case, the code needs, amongst others, the following improvements:

### Adapting the implementation

* Set up **testing** environment and create unit and end-to-end tests to assure long term stability
  and maintainability.
* Adapt the model to store and use matrix based weights and use **matrix operations** (e.g. matrix
  multiplication), in order to calculate the prediction. The field is already of SQLAlchemy
  `PickledType` and, thus, is also capable of storing matrices or any other structure needed.
* Adapt the methods in the model to handle matrix operations.
* Store model types in the database (maybe the field `model_name` can be used to achieve this?) and
  use a **model factory** to instantiate sub-classes of the model, overriding the 
  `calculate_estimate()` and/or `predict()` methods.
* Implement improved **authentication** module to enhance user experience.
* Use `redis` to store sessions on server side and **overcome security concerns of JWT**.

### Scaling the architecture

For two suggestions regarding the systems architecture see below (c.f. Fig 1 and Fig 2).

* First, the **prediction model API** and the authentication API need to be moved to a **separate 
  micro-services**. Futhermore, the prediction model API needs to be scaled horizontally, because
  the model prediction consumes a high amount of memory.
* In order to keep authentication decoupled, I suggest using an **API gateway** handling the 
  verification of the user authentication and forwarding the requests to the horizontally scaled
  prediction model API including the user data. The API gateway is part of both architectures
  suggested (c.f. Fig 1 and Fig 2).
* Another **bottleneck** might be the **interaction between micro-service and database**, because
  the most resource intense network activity is loading the data. Possibilities to tackle this
  challenge are:
    1. A **transparent distributed database service** responsible for data management and
       synchronization (c.f. Fig 1)
        + Pro: No routing based on user/model properties to a specific instance necessary, i.e. 
          efficient utilization of prediction model API instances
        + Con: Overhead of setting up and maintaining the distributed database system
    2. **Instance private database services** for the individual instances and API gateway being
       responsible to route the requests to the instance being responsible for the user or model
       (c.f. Fig 2)
        + Pro: Direct fast access to the database and no need for the overhead of a distributed
          database system
        + Pro: Might be advantageous and feasable, because the users use the same model again and
          again and there is no need for the availabilty of the model to other users
        + Con: Need to implement request distribution in API gateway and store the association of
          the instances to the users/models, possibly in a database/redis

![Transparent distributed database service](images/architecture-transparent-distributed-db.png)
*Fig 1: Transparent distributed database service for the prediction model API micro-service instances*

![Instance private database services](images/architecture-instance-private-db.png)
*Fig 2: Instance private database services for the prediction model API micro-service instances*
