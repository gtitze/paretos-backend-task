'''Assembles the application and provides the measures to let it run.'''

from os import getenv

from flask import Flask
from flask_restx import Api

from predicta.database import db
from predicta.prediction_model.api import ns as prediction_model_api
from predicta.auth.api import ns as auth_api, jwt

app = Flask(__name__)

# Load configuration from environment variables
db_uri = getenv('SQLALCHEMY_DATABASE_URI')
if db_uri:
    app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
app.config['JWT_SECRET_KEY'] = getenv('JWT_SECRET_KEY', 'Unsecure, set secure value in production.')

# Initialize the database with the app and create the tables
db.init_app(app)
db.create_all(app=app)

# Initialize jwt with the app
jwt.init_app(app)

# Setup the api
api = Api(app,
        title='Predicta API',
        description='Predicts models',
        doc=getenv('PREDICTA_DOC_PATH', None) or False
)
api.add_namespace(prediction_model_api, path= '')
api.add_namespace(auth_api, path= '')

# Make app directly executable
if __name__ == '__main__':
    app.run(debug=True)
