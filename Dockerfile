# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt requirements.txt
COPY app.py app.py
COPY predicta predicta

RUN python3 -m venv venv
RUN source venv/bin/activate
RUN pip install -r requirements.txt

EXPOSE 5000
CMD [ "python3", "-m", "flask", "run", "--host=0.0.0.0" ]
