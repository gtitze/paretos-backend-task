'''Provides the database object for its import into other parts of the application.'''

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
