'''Contains the model and the business loginc for the authentication.'''
import bcrypt

from flask_jwt_extended import create_access_token
from werkzeug.exceptions import Conflict, Unauthorized
from sqlalchemy.exc import IntegrityError

from predicta.database import db

class User(db.Model):
    '''Database model for a user and his/her authentication'''

    __tablename__ = 'user'
    user_name = db.Column(db.String(80), primary_key=True)
    password_hash = db.Column(db.LargeBinary)

    @classmethod
    def register(cls, user_name, password):
        '''Register a new user and return it.'''
        try:
            password_hash = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())
            user_obj = cls(user_name=user_name, password_hash=password_hash)
            db.session.add(user_obj)
            db.session.commit()
            return user_obj
        except IntegrityError as err:
            raise Conflict from err

    @classmethod
    def login(cls, user_name, password):
        '''Login as a user and return username and access token'''
        user_obj = cls.query.filter_by(user_name=user_name).first()
        if user_obj is None:
            raise Unauthorized()
        if not bcrypt.checkpw(password.encode('utf8'), user_obj.password_hash):
            raise Unauthorized()
        return {
            'user_name': user_name,
            'access_token': create_access_token(identity=user_name)
        }
