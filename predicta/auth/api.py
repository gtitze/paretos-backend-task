'''Creates the API namespace, routing and OpenAPI documentation for authentication.'''
# pylint: disable=no-self-use

from flask import request
from flask_restx import Resource, Namespace, fields, marshal
from flask_jwt_extended import JWTManager

from .models import User

jwt = JWTManager()

ns = Namespace('auth', 'Authentication')

user_fields = ns.model('User', {
    'user_name': fields.String,
    'password': fields.String,
})

user_data_fields = ns.model('UserData', {
    'user_name': fields.String,
})

auth_data_fields = ns.model('AuthData', {
    'user_name': fields.String,
    'access_token': fields.String,
})

@ns.route('/register')
class RegisterResource(Resource):
    '''Resource creating a RESTful API for registering a user'''

    @ns.doc('register_user')
    @ns.expect(user_fields)
    @ns.response(200, 'Success', user_data_fields)
    @ns.response(406, 'Conflict', auth_data_fields)
    def post(self):
        '''Registers a new user and return its user data.'''
        user_data = request.get_json(force=True)
        user_obj = User.register(**user_data)
        return marshal(user_obj, user_data_fields)

@ns.route('/login')
class LoginResource(Resource):
    '''Resource creating a RESTful API for logging in as a user'''

    @ns.doc('login_user')
    @ns.response(200, 'Success', auth_data_fields)
    @ns.response(401, 'Unauthorized', auth_data_fields)
    @ns.expect(user_fields)
    def post(self):
        '''Login as a user and return username and access token.'''
        user_data = request.get_json(force=True)
        user_obj = User.login(**user_data)
        return marshal(user_obj, auth_data_fields)
