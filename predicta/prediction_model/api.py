'''Creates the API namespace, routing and OpenAPI documentation for prediction models.'''
# pylint: disable=no-self-use

import collections

from werkzeug.exceptions import BadRequest

from flask import request
from flask_restx import Resource, Namespace, fields, marshal
from flask_jwt_extended import get_jwt_identity, verify_jwt_in_request

from .models import Model

ns = Namespace('model', 'Prediction Models')

model_fields = ns.model('Model', {
    'model_id': fields.Integer,
    'model_name': fields.String,
    'input_columns': fields.List(fields.String),
    'input_weights': fields.List(fields.Float),
})

model_data_fields = ns.model('ModelData', {
    'model_name': fields.String,
    'input_columns': fields.List(fields.String),
    'input_weights': fields.List(fields.Float),
})

prediction_data_fields = ns.model('PredictionData', {
    '<column_name_1>': fields.Float,
    '<column_name_2>': fields.Float,
})

prediction_fields = ns.model('Prediction', {
    'model_name': fields.String,
    'prediction_value': fields.Float,
})

@ns.route('')
@ns.response(401, "Unauthorized")
class ModelListResource(Resource):
    '''Model list resource creating a RESTful API for listing and creating prediction models'''

    @ns.doc('get_model_list')
    @ns.response(200, "Success", [model_fields])
    def get(self):
        '''Get all available prediction models.'''
        verify_jwt_in_request()
        user_name = get_jwt_identity()
        models_list = Model.list(user_name=user_name)
        return marshal(models_list, model_fields)

    @ns.response(200, "Success", [model_fields])
    @ns.response(404, "Bad Request")
    @ns.expect(model_data_fields, validate=True)
    def post(self):
        '''Create a prediction model.'''
        verify_jwt_in_request()
        user_name = get_jwt_identity()
        data = request.get_json(force=True)
        model_obj = Model.create(user_name, **data)
        return marshal(model_obj, model_fields)

@ns.route('/<int:model_id>')
@ns.doc(params={'model_id': 'The id of the model'})
@ns.response(200, "Success", [model_fields])
@ns.response(401, "Unauthorized")
@ns.response(403, "Forbidden")
class ModelResource(Resource):
    '''Model resource creating a RESTful API for operations on a certain prediction model'''

    @ns.response(404, "Not Found")
    def get(self, model_id):
        '''Get a prediction model by the model_id.'''
        verify_jwt_in_request()
        user_name = get_jwt_identity()
        model_obj = Model.get(user_name=user_name, model_id=model_id)
        return marshal(model_obj, model_fields)

    def delete(self, model_id):
        '''Delete a prediction model by the model_id.'''
        verify_jwt_in_request()
        user_name = get_jwt_identity()
        model_obj = Model.delete(user_name, model_id)
        return marshal(model_obj, model_fields)

@ns.route('/<int:model_id>/prediction')
@ns.doc(params={'model_id': 'The id of the model'})
class PredictionResource(Resource):
    '''Prediction resource creating a RESTful API for the prediction'''

    @ns.expect(prediction_data_fields)
    @ns.response(200, "Success", prediction_fields)
    @ns.response(400, "Bad Request")
    @ns.response(401, "Unauthorized")
    @ns.response(403, "Forbidden")
    def post(self, model_id):
        """Create a prediction from a model by the model_id."""
        verify_jwt_in_request()
        user_name = get_jwt_identity()
        obj_model = Model.get(user_name, model_id)
        data = request.get_json(force=True)
        if not isinstance(data, collections.Mapping):
            raise BadRequest('Request data must be a dictionary/object with float values')
        for input_value in data.values():
            if not isinstance(input_value, float):
                raise BadRequest('input_values for all columns (also ignored ones) must be float.')
        return obj_model.predict(**data)
