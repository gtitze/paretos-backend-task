'''This package contains the calculation functions for the respective models.'''

def weighted_sum(input_values, input_weights):
    '''Calculate a weighted sum based on a list of input values and a list of their weights.'''
    if len(input_values) != len(input_weights):
        raise ValueError('input_values and input_weights must be of same length')
    return sum([value * input_weights[i] for i, value in enumerate(input_values)])

def weighted_average(input_values, input_weights):
    '''Calculate the weighted average based on a list of input values and their weights.'''
    return weighted_sum(input_values, input_weights)/sum(input_weights)
