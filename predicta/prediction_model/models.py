'''Contains the models and the business loginc for the prediction models.'''

from werkzeug.exceptions import BadRequest, Forbidden, NotFound

from predicta.database import db
from predicta.prediction_model import calculation

class Model(db.Model):
    '''Database model for a prediction model'''

    __tablename__ = 'model'
    model_id = db.Column(db.Integer, primary_key=True)
    model_name = db.Column(db.String(80))
    user_name = db.Column(db.String(80))
    input_columns = db.Column(db.PickleType)
    input_weights = db.Column(db.PickleType)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if len(self.input_columns) != len(self.input_weights):
            raise BadRequest('input_columns and input_weights must be of same length')

    @classmethod
    def list(cls, user_name):
        '''List all prediction models for a user.'''
        return cls.query.filter_by(user_name=user_name).all()

    @classmethod
    def get(cls, user_name, model_id):
        '''Get a prediction model by id, if it belongs to the user.'''
        model_obj = cls.query.filter_by(model_id=model_id).one_or_none()
        if model_obj is None:
            raise NotFound()
        if model_obj.user_name != user_name:
            raise Forbidden()
        return model_obj

    @classmethod
    def create(cls, user_name, /, **kwargs):
        '''Create a prediction model for a user.'''
        model_obj = cls(user_name=user_name, **kwargs)
        db.session.add(model_obj)
        db.session.commit()
        return model_obj

    @classmethod
    def delete(cls, user_name, model_id):
        '''Delete a prediction model by id, if it belongs to the user.'''
        model_obj = cls.query.filter_by(user_name=user_name, model_id=model_id).one_or_none()
        if model_obj is None:
            raise NotFound()
        if model_obj.user_name != user_name:
            raise Forbidden()
        db.session.delete(model_obj)
        db.session.commit()
        return model_obj

    def calculate_estimate(self, input_values):
        '''Calculate the prediction from the input values and the input weights of the model.'''
        return calculation.weighted_sum(input_values, self.input_weights)

    def predict(self, **data):
        '''Create a prediction from data for the model.'''
        input_values = [data.get(str(col), 0.0) for col in self.input_columns]
        prediction_value = self.calculate_estimate(input_values)
        return {
            'model_name': self.model_name,
            'prediction_value': prediction_value,
        }
